# NETWORK IMPORT PARSER
****
**Private Library**
****
This Library using for parse xml file with network.uloop.com
for [uloop](https://www.uloop.com), [collegerentals](https://www.uloop.com), [collegestudentsapartments](https://www.uloop.com)
****

Method of use
````php
require_once 'vendor/autoload.php';

use NetworkImport\NetworkSetting;
use NetworkImport\NetworkParser;
use NetworkImport\FileReader;
use NetworkImport\FileStreamer;

/* INIT SETTINGS */
NetworkSetting::init($argv[1], 'path to download directory');
NetworkSetting::initAvailableImports([ /*'NAME_OF_IMPORT' => ITEM_POWERED_BY, ...*/ ]);
NetworkSetting::domainLimitSet([]);
NetworkSetting::initSubcategory([ /*'NAME_OF_IMPORT' => SUBCATEGORY_ID,*/ ], DEFAULT_VALUE);
/* INIT SETTINGS END */

// checkin if was all settings installing
NetworkSetting::invariant()->reject(function ($exception) {
    var_dump($exception);
    die();
});


try {
    $fileReader = FileReader::getContent(NetworkSetting::parseFile()); // or FileStreamer
} catch (Exception $e) {exit();}

$fileReader->parse(function (NetworkParser $parser) {
    // processing of results
});

$fileReader->catchError(function (Exception $exception) {
    var_dump($exception);// if we have error in your callback in $fileReader->parse
});
````
You can use while
````php
try {
    // or FileReader
    $fileReader = \NetworkImport\FileStreamer::getContent(\NetworkImport\NetworkSetting::parseFile());
} catch (Exception $e) {exit();}

while ($node = $fileReader->spitOutNode()) {
    // processing of results
}


````
`$argv[1] - NAME_OF_IMPORT`

### **To connect the library to the project**

Add this code to composer.json

For example

````json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://bitbucket.org/accbox/uloopnetworkimportreader.git"
    }
  ],
  "require": {
    "accbox/uloopnetworkimportreader": "dev-master"
  }
}
````

## **DOCUMENTATION**

### 1. Class - `\NetworkImport\NetworkSetting`
The class is used to initialize all the necessary data that is needed for 
successful parsing of hml files on sites 
[uloop](https://www.uloop.com), [collegerentals](https://www.uloop.com), [collegestudentsapartments](https://www.uloop.com)
    
#### Methods: 
`init`
````php
\NetworkImport\NetworkSetting::init('feed key', 'path to download directory');
````
- First parameter - Key of feed on network.uloop.com/feed-import-files/{key_of_feed}
- Second parameter - the path to the folder on your server in which, if necessary, the downloaded files for the parse will be saved
on MacOS for example - '/Users/User/Downloads/'

`initAvailableImports`
````php
\NetworkImport\NetworkSetting::initAvailableImports([ /*'NAME_OF_IMPORT' => ITEM_POWERED_BY, ...*/ ]);
````
An array in which the keys will be the names of the import and the values of their id

`domainLimitSet`
````php
\NetworkImport\NetworkSetting::domainLimitSet([]);
````
set domain limit

`initSubcategory`
````php
\NetworkImport\NetworkSetting::initSubcategory([ /*'NAME_OF_IMPORT' => SUBCATEGORY_ID,*/ ], 'DEFAULT_VALUE');
````
An array of keys in which the names of categories will be and the values of their id.

Categories that can be in the file: apartments,houses, sublets

`invariant`
````php
\NetworkImport\NetworkSetting::invariant()->reject(function ($exception) {
    var_dump($exception);
    die();
});
````
Method invariant check if was all settings installing.

If not, an exception will be raised which is handled in the method `reject`


### 2. Class - `\NetworkImport\FileReader`
Using XMLReader for parsing
````php
$fileReader = \NetworkImport\FileReader::getContent(\NetworkImport\NetworkSetting::parseFile());
$fileReader->parse(function (\NetworkImport\NetworkParser $parser) {
    // processing of results
});
````
Have catchError method
### 3. Class - `\NetworkImport\FileStreamer`
Using XmlStringStreamer for parsing.
You can parse only file, not url.
First the file is downloaded and then parsed.
````php
$fileReader = \NetworkImport\FileStreamer::getContent(\NetworkImport\NetworkSetting::parseFile());
$fileReader->parse(function (\NetworkImport\NetworkParser $parser) {
   throw new Exception('Some error');
});
````
Have catchError method

### 4. Class - `\NetworkImport\NetworkParser`
Contains methods that return the listing field.

An instance of this class is passed to callback which is called below.
````php
$fileReader = \NetworkImport\FileReader::getContent(\NetworkImport\NetworkSetting::parseFile());
$fileReader->parse(function (\NetworkImport\NetworkParser $parser) {
    // processing of results
});
````

If we have throw in $fileReader->parse(callback).

We can  process it as shown in the example below

````php
$fileReader = \NetworkImport\FileStreamer::getContent(\NetworkImport\NetworkSetting::parseFile());
$fileReader->parse(function (\NetworkImport\NetworkParser $parser) {
   throw new Exception('Some error');
});
$fileReader->catchError(function (Exception $exception) {
    var_dump($exception->getMessage());
    // will print Some error
});
````

   
   