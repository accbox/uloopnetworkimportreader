<?php


namespace NetworkImport\Reader;


use Exception;

abstract class Reader
{
    /**
     * @var bool
     */
    protected $haveError = false;

    /**
     * @var Exception
     */
    protected $exception;

    /**
     * @var
     */
    protected $xml;

    /**
     * FileReader constructor.
     * @param $xml
     */
    public function __construct($xml)
    {
        $this->xml = $xml;
    }

    /**
     * @param Exception $exception
     */
    protected function setError(Exception $exception)
    {
        $this->haveError = true;
        $this->exception = $exception;
    }

    /**
     * @param string $xml
     * @return false|\SimpleXMLElement
     */
    protected final function simplexmlLoadString($xml)
    {
        return simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA | LIBXML_NOWARNING | LIBXML_NOBLANKS);
    }

    /**
     * @param \Closure $closure
     * @return Reader
     */
    public function catchError(\Closure $closure)
    {
        $this->haveError && !empty($this->exception) && is_callable($closure) && $closure($this->exception);
        $this->haveError = false;
        $this->exception = null;

        return $this;
    }

    /**
     * @param \Closure $closure
     * @return Reader
     */
    public function errorRegister(\Closure $closure)
    {
        register_shutdown_function(function () use ($closure) {
            $error = error_get_last();
            $error['type'] === E_ERROR && $closure($error);
        });

        return $this;
    }

    /**
     * @param \Closure $callback
     * @return $this
     */
    abstract public function parse(\Closure $callback);
}