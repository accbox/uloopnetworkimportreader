<?php

namespace NetworkImport;

use Exception;
use NetworkImport\Validate\NetworkValidator;

/**
 * Class NetworkSetting
 */
class NetworkSetting
{
    private static $availableImports;
    private static $currentImportName;
    private static $uploadFolder;
    private static $domainLimit;
    private static $subcategoryArray;
    private static $typeFeedsArray;
    private static $typeFeedsDefault;
    private static $channelIdsArray;
    private static $channelIdDefault;

    /**
     * @return int|mixed
     */
    public static function domainLimit()
    {
        if (!is_null(self::$domainLimit) && is_array(self::$domainLimit) && isset(self::$domainLimit[self::currentPoweredBy()])) {
            return self::$domainLimit[self::currentPoweredBy()];
        }

        if (!is_null(self::$domainLimit) && is_array(self::$domainLimit) && isset(self::$domainLimit[self::parseFile()])) {
            return self::$domainLimit[self::parseFile()];
        }

        return 300;
    }

    /**
     * @param $subcategory
     * @return string
     * @throws Exception
     */
    public static function returnSubcategory($subcategory)
    {
        if (isset(self::$subcategoryArray[$subcategory])) {
            return self::$subcategoryArray[$subcategory];
        }

        $error = 'Category "' . $subcategory . '" is not available' . PHP_EOL;
        $error .= 'You need call NetworkSetting::existCategory() and skip wrong categories' . PHP_EOL;
        $error .= 'Or add "' . $subcategory . '" in NetworkSetting::initSubcategory';

        throw new Exception($error);
    }

    /**
     * @param $subcategory
     * @return bool
     */
    public static function existCategory($subcategory)
    {
        return isset(self::$subcategoryArray[$subcategory]);
    }

    /**
     * @return mixed
     */
    public static function currentPoweredBy()
    {
        return self::$availableImports[self::$currentImportName];
    }

    /**
     * @return string
     */
    public static function parseFile()
    {
        return trim(self::$currentImportName);
    }

    /**
     * @return string
     */
    public static function uploadFolder($additionalPath = '')
    {
        return rtrim(self::$uploadFolder, '/') . '/' . ltrim($additionalPath, '/');
    }

    /**
     * @return mixed
     */
    public static function typeFeed()
    {
        return isset(self::$typeFeedsArray[self::currentPoweredBy()])
            ? self::$typeFeedsArray[self::currentPoweredBy()]
            : (!is_null(self::$typeFeedsDefault) ? self::$typeFeedsDefault : '');
    }

    /**
     * @return mixed
     */
    public static function channelId()
    {
        return !empty(self::$channelIdsArray) && isset(self::$channelIdsArray[self::currentPoweredBy()])
            ? self::$channelIdsArray[self::currentPoweredBy()]
            : (!is_null(self::$channelIdDefault) ? self::$channelIdDefault : '');
    }

    /**
     * @param $name
     * @param $uploadFolder
     */
    public static function init($name, $uploadFolder)
    {
        self::$currentImportName = $name;
        self::$uploadFolder = $uploadFolder;
        ini_set('memory_limit', '2000M');
    }

    /**
     * @param $importsArray
     */
    public static function initAvailableImports($importsArray)
    {
        self::$availableImports = $importsArray;
    }

    /**
     * @param $subcategoryArray
     */
    public static function initSubcategory($subcategoryArray)
    {
        if (is_array($subcategoryArray)) {
            self::$subcategoryArray = $subcategoryArray;
        }
    }


    /**
     * @return NetworkValidator
     */
    public static function invariant()
    {
        try {
            if (!self::$currentImportName || !isset(self::$availableImports[self::$currentImportName])) {
                throw new Exception('Error: import name is not valid. Watch to initAvailableImports');
            }

            if (!self::$subcategoryArray) {
                throw new Exception('Error: Please Install subcategory settings');
            }

            if (!self::$uploadFolder) {
                throw new Exception('Error: $uploadFolder is not exist');
            }

            if ((is_null(self::$typeFeedsArray) && empty(self::$typeFeedsArray)) && is_null(self::$typeFeedsDefault)) {
                throw new Exception('Error: You need call initFeedTypeSettings');
            }

            if ((is_null(self::$channelIdsArray) && empty(self::$channelIdsArray)) && is_null(self::$channelIdDefault)) {
                throw new Exception('Error: You need call initChannelId');
            }

            return new NetworkValidator(null);
        } catch (\Exception $exception) {
            return new NetworkValidator($exception);
        }
    }

    /**
     * @param $domainLimit
     */
    public static function domainLimitSet($domainLimit)
    {
        self::$domainLimit = $domainLimit;
    }

    /**
     * @param array $array
     * @param $default
     */
    public static function initFeedTypeSettings($array, $default)
    {
        self::$typeFeedsArray = is_array($array) && !empty($array) ? $array : [];
        self::$typeFeedsDefault = $default;
    }

    /**
     * @param $array
     * @param $default
     */
    public static function initChannelId($array, $default)
    {
        self::$channelIdsArray = is_array($array) && !empty($array) ? $array : [];
        self::$channelIdDefault = $default;
    }
}