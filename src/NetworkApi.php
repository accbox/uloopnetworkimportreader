<?php

namespace NetworkImport;

class NetworkApi
{
    private $url = 'https://network.uloop.com';

    /**
     * @param $fileName
     * @return string
     */
    public function feedImportFileApiLink($fileName)
    {
        return "$this->url/feed-import-files/$fileName";
    }

    /**
     * @param $file
     * @return false|string
     */
    public function getFile($file)
    {
        return file_get_contents(
            $this->feedImportFileApiLink($file)
        );
    }
}