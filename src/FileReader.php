<?php

namespace NetworkImport;

use Closure;
use Exception;
use NetworkImport\Reader\Reader;
use XMLReader;

class FileReader extends Reader
{
    /**
     * @param $file
     * @return static
     * @throws Exception
     */
    public static function getContent($file)
    {
        return self::instance((new NetworkApi())->feedImportFileApiLink($file));
    }

    /**
     * @param $url
     * @return static
     * @throws Exception
     */
    public static function getContentFromPathOrUrl($url)
    {
        return self::instance($url);
    }


    /**
     * @param Closure $callback
     * @return FileReader
     */
    public function parse(Closure $callback)
    {
        try {
            while ($this->xml->read()) {
                if ($this->xml->nodeType == XMLReader::ELEMENT && $this->xml->name == 'ad') {
                    $product_xml = $this->xml->readOuterXml();
                    $callback(new NetworkParser($this->simplexmlLoadString($product_xml)));
                    unset($product_xml);
                }
            }
        } catch (Exception $exception) {
            $this->setError($exception);
        }

        return $this;
    }

    /**
     * @param $path
     * @return static
     * @throws Exception
     */
    private static function instance($path)
    {
        if (!class_exists('XMLReader')) {
            throw new Exception('Class XMLReader is not exist');
        }

        $xml = new XMLReader();
        if (!$xml->open($path)) {
            throw new Exception("Error: $path was not opening");
        }

        return new static($xml);
    }



    /**
     * return instance of NetworkParser
     *
     * if you want use parser with "while"
     *
     * for example
     * while ($instance = $a->spitOutNode()) {
     *    var_dump($a->title());
     * }
     *
     * @return NetworkParser|false
     */
    public function spitOutNode()
    {
        while ($this->xml->read()) {
            if ($this->xml->nodeType == XMLReader::ELEMENT && $this->xml->name == 'ad') {
                $product_xml = $this->xml->readOuterXml();
                return new NetworkParser($this->simplexmlLoadString($product_xml));
            }
        }

        return false;
    }

}