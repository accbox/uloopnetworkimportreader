<?php

namespace NetworkImport;

use NetworkImport\Parser\AmenityObj;
use NetworkImport\Parser\OfficeHours;
use NetworkImport\Parser\ParseImport;
use NetworkImport\Parser\TourObj;
use NetworkImport\Parser\UnitObj;

class NetworkParser extends ParseImport
{
    private $units;

    /**
     * @inheritDoc
     */
    public function id()
    {
        return isset($this->property['@attributes']) && isset($this->property['@attributes']['id'])
            ? $this->property['@attributes']['id']
            : '';
    }

    /**
     * @inheritDoc
     */
    public function subcategory()
    {
        return $this->returnProperty('subcategory', '');
    }

    /**
     * if category is not exist in NetworkSetting
     * @return bool
     */
    public function skipSubcategory()
    {
        return !NetworkSetting::existCategory($this->subcategory());
    }

    /**
     * @inheritDoc
     */
    public function code()
    {
        return $this->returnProperty('code', '');
    }

    /**
     * @inheritDoc
     */
    public function label()
    {
        return $this->returnProperty('label', '');
    }

    /**
     * @inheritDoc
     */
    public function poweredBy()
    {
        return NetworkSetting::currentPoweredBy();
    }

    /**
     * @inheritDoc
     */
    public function title()
    {
        $title = $this->returnProperty('title', '');

        if (!empty($title)) {
            return $title;
        }

        // if title wrapped in CDATA tag
        if (!isset($this->libXmlProperty->title)) {
            return '';
        }

        $title = $this->libXmlProperty->title->__toString();

        return !empty($title) ? $title : '';
    }

    /**
     * @inheritDoc
     */
    public function description()
    {
        return $this->property['content'] && !is_array($this->property['content']) && (string) $this->property['content'] !== 'Array'
            ? nl2br(trim(str_replace('â', "'",(string) $this->property['content'])))
            : '';
    }

    /**
     * @inheritDoc
     */
    public function address()
    {
        return $this->returnProperty('address', '');
    }

    /**
     * @inheritDoc
     */
    public function city()
    {
        return $this->returnProperty('city', '');
    }

    /**
     * @inheritDoc
     */
    public function region()
    {
        return $this->returnProperty('region', '');
    }
    
    /**
     * @inheritDoc
     */
    public function media()
    {
        return $this->returnProperty('media', '');
    }
    
    /**
     * @inheritDoc
     */
    public function mediaPreview()
    {
        return $this->returnProperty('media_preview', '');
    }

    /**
     * @inheritDoc
     */
    public function routeEmail()
    {
        return (string) $this->returnProperty('route_email', '');
    }

    /**
     * @inheritDoc
     */
    public function longitude()
    {
        return (float) $this->returnProperty('longitude', 0);
    }

    /**
     * @inheritDoc
     */
    public function latitude()
    {
        return (float) $this->returnProperty('latitude', 0);
    }

    /**
     * @inheritDoc
     */
    public function zipCode()
    {
        return $this->returnProperty('postcode', '');
    }

    /**
     * @inheritDoc
     */
    public function pictures()
    {
        if (!isset($this->property['pictures']) || !isset($this->property['pictures']['picture'])) {
            return [];
        }

        return is_array($this->property['pictures']['picture']) && count($this->property['pictures']['picture'])
            ? $this->property['pictures']['picture']
            : [$this->property['pictures']['picture']];
    }

    /**
     * @inheritDoc
     */
    public function units()
    {
        if (isset($this->units) && is_array($this->units)) {
            return $this->units;
        }

        if (!isset($this->property['units']) || !isset($this->property['units']['unit']) || empty($this->property['units']['unit'])) {
            return [];
        }

        if (!isset($this->property['units']['unit'][0])) {
            $this->property['units']['unit'] = [$this->property['units']['unit']];
        }

        $this->units = array_map(function ($unit) {
            return new UnitObj($unit);
        }, $this->property['units']['unit']);

        return $this->units;
    }

    /**
     * @return array
     */
    public function unitPhotosWithoutFirsOfUnits()
    {
        $photo = [];

        foreach ($this->units() as $unit) {
            foreach ($unit->picture as $k => $item) {
                if ($k === 0) {
                    continue;
                }

                $photo[] = [
                    'photo' => $item,
                    'smallPhoto' => isset($unit->smallPicture[$k]) && $unit->smallPicture[$k] ? $unit->smallPicture[$k] : $item
                ];
            }
        }

        return $photo;
    }
    
    /**
     * @return string
     */
    public function managerID()
    {
        return (string) $this->returnProperty('manager_id', '');
    }

    /**
     * @return string
     */
    public function manager()
    {
        return (string) $this->returnProperty('manager', '');
    }

    /**
     * @inheritDoc
     */
    public function amenities($callback = null)
    {
        if (!isset($this->libXmlProperty->amenities) || !isset($this->libXmlProperty->amenities->amenity)) {
            return [];
        }

        if (!$this->libXmlProperty->amenities->amenity->count()) {
            return  [];
        }

        $amenities = (array) $this->libXmlProperty->amenities->amenity;

        if (!count($amenities)) {
            return [];
        }

        $amenities = [];
        foreach ($this->libXmlProperty->amenities->amenity as $amenity) {
            !is_array($amenity) && is_a($amenity, 'SimpleXMLElement') && $amenities[] = $amenity;
        }

        $amenities = array_map(function ($amenity) {
            return new AmenityObj($amenity);
        }, $amenities);

        $amenities = array_filter($amenities, function ($amenity) {
            return $amenity->isValid();
        });

        if (isset($callback) && is_callable($callback)) {
            return $callback($amenities);
        }

        return $amenities;
    }

    /**
     * return string with route url or empty string
     * with tag <origin_url>
     * @return string
     */
    public function routeUrl()
    {
        return (string) $this->returnProperty('origin_url', '');
    }

    /**
     * return string with website_url or empty string
     * @param null|string|array $additionalParams
     * @return string
     */
    public function websiteUrl($additionalParams = null)
    {
        $url = (string) $this->returnProperty('website_url', '');
        if (empty($url)) {
            return '';
        }

        if (is_null($additionalParams)) {
            return $url;
        }

        if (is_string($additionalParams)) {
            return $url . (strpos($url, '?') === false ?  '?' : '&') . ltrim($additionalParams, '&');
        }

        if (is_array($additionalParams)) {
            return $url . (strpos($url, '?') === false ?  '?' : '&') . http_build_query($additionalParams);
        }

        return $url;
    }

    /**
     * return tag <website_url_text>
     * @return string
     */
    public function websiteUrlText()
    {
        return (string) $this->returnProperty('website_url_text', '');
    }

    /**
     * return string with phone number or empty string
     *
     * @return mixed|string
     */
    public function phone()
    {
        return $this->returnProperty('phone', '');
    }

    /**
     * @return mixed|string
     */
    public function features($json = false)
    {
        $value = $this->getPropertyByPath('features/feature', '');

        if ($value && $json) {
            $value = json_encode($value);
            return $value ? $this->trueFalseInStringReplace($value) : '';
        }

        return $value;
    }

    /**
     * converting additional params
     * @return string
     */
    public function preparedFeatures()
    {
        return (string) $this->returnProperty('prepared_features', '');
    }

    /**
     * get multiple bath array of units
     *
     * @param \Closure|null $callback
     *  multipleBathArray(function ($bedrooms) { return $bedrooms; })
     *
     * @return array
     */
    public function multipleBedsArray(\Closure $callback = null)
    {
        $multipleBedsArray = array_unique(array_map(function ($unit) use ($callback) {
            return isset($callback) && is_callable($callback) ? $callback($unit->bedrooms) : $unit->bedrooms;
        }, $this->units()));

        // 0 is not empty value
        return array_filter($multipleBedsArray, function ($item) {
            return !empty($item) || (is_int($item) && $item === 0 || is_string($item) && $item === '0');
        });
    }

    /**
     * @inheritDoc
     */
    public function multipleBathArray(callable $callback = null)
    {
        $multipleBathArray = array_unique(array_map(function ($unit) use ($callback) {
            return isset($callback) && is_callable($callback) ? $callback($unit->bathrooms) : $unit->bathrooms;
        }, $this->units()));

        // 0 is not empty value
        return array_filter($multipleBathArray, function ($item) {
            return !empty($item) || (is_int($item) && $item === 0 || is_string($item) && $item === '0');
        });
    }


    /**
     * max rent of units
     * @return int|mixed
     */
    public function maxRent()
    {
        if (isset($this->property['max_rent'])) {
            return $this->property['max_rent'];
        }

        $rentArray = array_map(function ($unit) {
            return $unit->maxRent;
        }, $this->units());

        return count($rentArray) ? max($rentArray) : 0;
    }

    /**
     * min rent of units
     * @return int|mixed
     */
    public function minRent()
    {
        if (isset($this->property['min_rent']) && !empty($this->property['min_rent'])) {
            return $this->property['min_rent'];
        }

        $rentArray = array_map(function ($unit) {
            return $unit->minRent;
        }, $this->units());

        $rentArray = array_filter($rentArray, function ($rent) {
            return $rent;
        });

        return count($rentArray) ? min($rentArray) : 0;
    }


    /**
     * @inheritDoc
     */
    public function userId($site = '')
    {
        $userId = $this->returnProperty('user_id', null);

        if (!$userId || !is_array($userId)) {
            return null;
        }

        if (empty($site)) {
            return (array) $userId;
        }

        return isset($userId[$site]) && !empty($userId[$site]) ? (int) $userId[$site] : null;
    }

    /**
     * 1 or 0
     * @return int
     */
    public function featured()
    {
        return ((int) $this->returnProperty('featured', 0)) === 0 ? 0 : 1;
    }


    /**
     * @return string
     */
    public function specialNote()
    {
        return (string) $this->returnProperty('spec_note', '');
    }

    /**
     * @param int $default
     * @return float
     */
    public function maxDistance($default = 0)
    {
        return (float) $this->returnProperty('max_distance', $default);
    }

    /**
     * @return array|TourObj[]
     */
    public function tours(callable $prepareItemCallback = null)
    {
        $tours = $this->getPropertyByPath('tours/tour', []);

        if (empty($tours)) {
            return [];
        }

        /**
         * @var TourObj[] $tours
         */
        $tours = array_map(function ($item) {
            return new TourObj(
                isset($item['tour_url']) ? $item['tour_url'] : '',
                isset($item['tour_preview']) ? $item['tour_preview'] : ''
            );
        }, $tours);

        /**
         * @var TourObj[] $tours
         */
        $tours = array_filter($tours, function (TourObj $tour) {
            return $tour->isValid();
        });


        if (!is_null($prepareItemCallback) && is_callable($prepareItemCallback) && !empty($tours)) {
            return array_map(function (TourObj $tour) use ($prepareItemCallback) {
                return $prepareItemCallback($tour);
            }, $tours);
        }

        return !empty($tours) ? $tours : [];
    }

    /**
     * @return mixed|string
     */
    public function typeFeed()
    {
        return NetworkSetting::typeFeed();
    }

    /**
     * @return mixed|string
     */
    public function channelId()
    {
        return NetworkSetting::channelId();
    }

    /** @inheritDoc */
    public function officeHours()
    {
        $officeHours = $this->getPropertyByPath('office_hours/office_hour', []);

        if (empty($officeHours)) {
            return [];
        }

        $officeHours = array_map(function ($item) {
            return new OfficeHours($item);
        }, $officeHours);

        return array_filter($officeHours, function (OfficeHours $item) {
            return $item->isValid();
        });
    }

    /** @inheritDoc */
    public function mapStaticImages(callable $callback = null)
    {
        $mapStaticString = $this->returnProperty('map_static_images', []);

        if (empty($mapStaticString) || !isset($mapStaticString['image']) || !isset($mapStaticString['small'])) {
            return is_callable($callback) ? $callback([]) : [];
        }

        if (empty($mapStaticString['image']) || empty($mapStaticString['small'])) {
            return is_callable($callback) ? $callback([]) : [];
        }

        return $callback([$mapStaticString['image'], $mapStaticString['small']]);
    }

    /**
     * Return true if listing has Google Paid Photo
     * @return bool
     */
    public function issetGooglePhoto()
    {
        if (
            isset($this->libXmlProperty->pictures) &&
            isset($this->libXmlProperty->pictures->picture) &&
            is_a($this->libXmlProperty->pictures->picture, 'SimpleXMLElement')
        ) {
            foreach ($this->libXmlProperty->pictures->picture as $item) {
                if (isset($item->attributes()['google']) && (int) $item->attributes()['google']->__toString()) {
                    return true;
                }
            }
        }

        return false;
    }
}