<?php


namespace NetworkImport\Validate;


class NetworkValidator
{
    private $exception;

    public function __construct($exception)
    {
        $this->exception = $exception;
    }

    public function reject(\Closure $closure)
    {
        if (is_callable($closure) && !empty($this->exception) && is_a($this->exception, \Exception::class)) {
            $closure($this->exception);
        }
    }
}