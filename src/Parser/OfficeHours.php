<?php


namespace NetworkImport\Parser;

/**
 * @property string $day_of_week
 * @property string $time_start
 * @property string $time_end
 *
 *
 *
 * Class OfficeHours
 * @package NetworkImport\Parser
 */
class OfficeHours
{
    public $day_of_week;
    public $time_start;
    public $time_end;

    /**
     * OfficeHours constructor.
     * @param array $item
     */
    public function __construct($item)
    {
        $this->day_of_week = $this->isItemValid($item, 'day_of_week') ? $item['day_of_week'] : '';
        $this->time_start = $this->isItemValid($item, 'time_start')  ? $item['time_start'] : '';
        $this->time_end = $this->isItemValid($item, 'time_end') ? $item['time_end'] : '';
    }

    /**
     * @param $item
     * @param string $field
     * @return bool
     */
    private function isItemValid($item, $field)
    {
        return is_array($item) && isset($item[$field]) && !empty($item[$field]);
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return !empty($this->day_of_week) && !empty($this->time_start) && !empty($this->time_end);
    }
}