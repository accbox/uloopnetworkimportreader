<?php


namespace NetworkImport\Parser;

use NetworkImport\Parser\Units\Media;

class UnitObj
{
    /**
     * @var array|\SimpleXMLElement
     */
    private $unit;

    /**
     * @var string
     */
    public $id;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $description;

    /**
     * @var string
     */
    public $bedrooms;

    /**
     * @var string
     */
    public $bathrooms;

    /**
     * @var int
     */
    public $minRent;

    /**
     * @var int
     */
    public $maxRent;

    /**
     * @var int
     */
    public $minSize;

    /**
     * @var int
     */
    public $maxSize;

    /**
     * @var array
     */
    public $picture;

    /**
     * @var array
     */
    public $smallPicture;

    /**
     * @var string
     */
    public $availableDates;

    /**
     * @var Media
     */
    public $media;

    /**
     * UnitObj constructor.
     * @param $unit
     */
    public function __construct($unit)
    {
        $this->unit = $unit;
        $this->init();
    }

    /**
     * init unit fields
     */
    private function init()
    {
        $this->id = $this->setId();
        $this->title = (string) $this->setter('title');
        $this->description = (string) $this->setter('description');
        $this->bedrooms = (string) $this->setter('bedrooms', 0);
        $this->bathrooms = (string) $this->setter('bathrooms', '');
        $this->minRent = (int) $this->setter('minRent', 0);
        $this->maxRent = (int) $this->setter('maxRent', 0);
        $this->minSize = (int) $this->setter('minSize', 0);
        $this->maxSize = (int) $this->setter('maxSize', 0);
        $this->picture = (array) $this->setter('picture', []);
        $this->smallPicture = (array) $this->setter('smallPicture', []);
        $this->availableDates = (string) $this->setter('availableDates');
        $this->media = $this->setMedia();
    }

    /**
     * @return string
     */
    private function setId()
    {
        if (is_array($this->unit)) {
            return isset($this->unit['@attributes']) && isset($this->unit['@attributes']['id']) && !empty($this->unit['@attributes']['id'])
                ? (string) $this->unit['@attributes']['id']
                : '';
        }

        if (is_object($this->unit) || is_a($this->unit, 'SimpleXMLElement')) {
            return $this->unit->attributes() && $this->unit->attributes()->id
                ? (string) $this->unit->attributes()->id
                : '';
        }

        return '';
    }

    /**
     * @param $name
     * @param string $default
     * @return mixed|string
     */
    private function setter($name, $default = '')
    {
        if (is_object($this->unit)) {
            return isset($this->unit->$name) && !(empty($this->unit->$name) && $this->unit->$name !== 0)
                ? $this->unit->$name
                : $default;
        }

        return isset($this->unit[$name]) && !empty($this->unit[$name])
            ? $this->unit[$name]
            : $default;
    }


    /**
     * @return Media
     */
    private function setMedia()
    {
        $media = $this->setter('media', []);
        return new Media(!empty($media) ? $media : []);
    }
}