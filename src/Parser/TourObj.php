<?php


namespace NetworkImport\Parser;


class TourObj
{
    /**
     * @var string
     */
    public $url;

    /**
     * @var string
     */
    public $preview;

    public function __construct($url, $preview)
    {
        $this->url = is_string($url) ? $url : '';
        $this->preview = is_string($preview) ? $preview : '';
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return !empty($this->url) || !empty($this->preview);
    }
}