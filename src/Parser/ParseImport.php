<?php


namespace NetworkImport\Parser;


abstract class ParseImport
{
    /** @var \SimpleXMLElement */
    protected $libXmlProperty;

    /** @var array */
    protected $property;

    public function __construct($property)
    {
        $this->property = json_decode(json_encode($property), true);;
        $this->libXmlProperty = $property;
    }

    /**
     * @param $name
     * @param $default
     * @return mixed
     */
    protected function returnProperty($name, $default)
    {
        return $this->r($name, $default, $this->property);
    }

    /**
     * @param $name
     * @param $default
     * @param string $exploder
     * @return mixed
     */
    protected function getPropertyByPath($name, $default, $exploder = '/')
    {
        return $this->returnDataPropertyByPath($name, $default, $exploder, $this->property);
    }

    /**
     * @param $name
     * @param $default
     * @param string $exploder
     * @param null $property
     * @return mixed
     */
    private function returnDataPropertyByPath($name, $default, $exploder, $property)
    {
        if (is_null($property) || empty($property) || !is_array($property)) {
            return $default;
        }

        if (false === strpos($name, $exploder)) {
            return $this->r($name, $default, $property);
        }

        $names = explode($exploder, $name);
        $name = array_shift($names);

        return $this->returnDataPropertyByPath(
            implode($exploder, $names),
            $default,
            $exploder,
            $this->r($name, $default, $property)
        );
    }

    /**
     * return with $property
     * @param $name
     * @param $default
     * @param $property
     * @return mixed
     */
    protected function r($name, $default, $property)
    {
        if (!is_array($property)) {
            return $default;
        }

        if (!isset($property[$name])) {
            return $default;
        }

        return !empty($property[$name]) ? $property[$name] : $default;
    }

    /**
     * {"value":"true"} -> {"value":true}
     *
     * @param $string
     * @return array|string|string[]
     */
    protected function trueFalseInStringReplace($string)
    {
        return str_replace(
            ['"true"', '"false"'],
            ['true','false'],
            $string
        );
    }

    /**
     * @return array|OfficeHours[]
     */
    abstract public function officeHours();

    /**
     * @return mixed|string
     */
    abstract public function id();

    /**
     * @return mixed|string
     */
    abstract public function subcategory();

    /**
     * @return mixed|string
     */
    abstract public function code();

    /**
     * @return mixed|string
     */
    abstract public function label();

    /**
     * @return mixed|int
     */
    abstract public function poweredBy();

    /**
     * @return mixed
     */
    abstract public function title();

    /**
     * @return string
     */
    abstract public function description();

    /**
     * @return mixed
     */
    abstract public function address();

    /**
     * @return mixed
     */
    abstract public function city();

    /**
     * @return mixed
     */
    abstract public function region();

    /**
     * @return string
     */
    abstract public function routeEmail();

    /**
     * @return float
     */
    abstract public function longitude();

    /**
     * @return float
     */
    abstract public function latitude();

    /**
     * @return mixed|string
     */
    abstract public function zipCode();

    /**
     * @return array|mixed
     */
    abstract public function pictures();

    /**
     * @return array|UnitObj[]
     */
    abstract public function units();

    /**
     * return amenities
     *
     * @param null $callback
     * $callback = function(array $amenities) {
     *  // your code
     * }
     *  $amenities in callback is array of SimpleXMLElement objects
     * @return array|AmenityObj[]
     */
    abstract public function amenities($callback = null);

    /**
     * get multiple bath array of units
     *
     * @param callable|null $callback
     *  multipleBathArray(function ($bathrooms) { return $bathrooms; })
     *
     * @return array
     */
    abstract public function multipleBathArray(callable $callback = null);

    /**
     * return array with userId's
     * or return userId by $site
     * or return null
     *
     * @param string $site
     * @return array|int|null
     */
    abstract public function userId($site = '');

    /**
     *
     * @param callable|null $callback callback have one param. Param is array with ['image' => 'small' => ] or empty array
     * @return mixed|array|string  DEFAULT RETURN TYPE IS ARRAY
     */
    abstract public function mapStaticImages(callable $callback = null);
}