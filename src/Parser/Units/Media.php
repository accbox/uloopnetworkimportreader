<?php


namespace NetworkImport\Parser\Units;

/**
 * @property string $url
 * @property string $photo
 * @property string $small_photo
 *
 * Class Media
 * @package NetworkImport\Parser\Units
 */
final class Media
{
    /**
     * @var string
     */
    public $url, $photo, $small_photo;

    /**
     * Media constructor.
     * @param mixed|array|object $value
     */
    public function __construct($value)
    {
        if (empty($value)) {
            return;
        } elseif (is_array($value)) {
            $this->fromArray($value);
        } elseif (is_object($value)) {
            $this->fromObject($value);
        }

        if (isset($this->photo) && isset($this->small_photo)) {
            empty($this->small_photo) && !empty($this->photo) && $this->small_photo = $this->photo;
            empty($this->photo) && !empty($this->small_photo) && $this->photo = $this->small_photo;
        }
    }

    /**
     * @param object $value
     */
    private function fromObject($value)
    {
        $this->url = isset($value->url) && !empty($value->url) ? $value->url : '';
        $this->photo = isset($value->photo) && !empty($value->photo) ? $value->photo : '';
        $this->small_photo = isset($value->small_photo) && !empty($value->small_photo) ? $value->small_photo : '';
    }

    /**
     * @param array $value
     */
    private function fromArray($value)
    {
        $this->url = isset($value['url']) && !empty($value['url']) ? $value['url'] : '';
        $this->photo = isset($value['photo']) && !empty($value['photo']) ? $value['photo'] : '';
        $this->small_photo = isset($value['small_photo']) && !empty($value['small_photo']) ? $value['small_photo'] : '';
    }

    /**
     * @param string $field
     * @return bool
     */
    private function valid($field)
    {
        return isset($this->$field) && !empty($this->$field) && is_string($this->$field);
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return $this->valid('url') && $this->valid('photo') && $this->valid('small_photo');
    }
}