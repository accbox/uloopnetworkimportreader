<?php


namespace NetworkImport\Parser;


class AmenityObj
{
    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $data;

    /**
     * AmenityObj constructor.
     * @param $amenity
     */
    public function __construct($amenity)
    {
        if (is_a($amenity, "SimpleXmlElement")) {
            $this->type = $amenity->attributes()->type && $amenity->attributes()->type->__toString()
                ? (string) $amenity->attributes()->type->__toString()
                : 'Other';

            $this->data = $amenity->__toString() ? $amenity->__toString() : '';
        }
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return !empty($this->type) && !empty($this->data);
    }
}