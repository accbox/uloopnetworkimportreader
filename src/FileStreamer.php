<?php

namespace NetworkImport;

use Exception;
use NetworkImport\Reader\Reader;
use Prewk\XmlStringStreamer;
use Prewk\XmlStringStreamer\Parser\UniqueNode;
use Prewk\XmlStringStreamer\Stream\File;

class FileStreamer extends Reader
{
    /**
     * @param $file
     * @param false $wget
     * @return FileStreamer
     * @throws Exception
     */
    public static function getContent($file, $wget = false)
    {
        $api_url = (new NetworkApi())->feedImportFileApiLink($file);
        return $wget ? self::createWgetStreamer($api_url) : self::createStreamer($api_url);
    }

    /**
     * @param $api_url
     * @param false $wget
     * @return FileStreamer
     * @throws Exception
     */
    public static function getContentByUrl($api_url, $wget = false)
    {
        return $wget ? self::createWgetStreamer($api_url) : self::createStreamer($api_url);
    }

    /**
     * This function uses file_get_contents for loading file.
     * And  file_put_contents uses for saving file.
     *
     * @param $url
     * @return FileStreamer
     * @throws Exception
     */
    public static function createStreamer($url)
    {
        $upload = NetworkSetting::uploadFolder('/' . NetworkSetting::parseFile() . '_network.xml');
        $content = file_get_contents($url);

        if (file_exists($upload)) {
            unlink($upload);
        }

        if ($content) {
            file_put_contents($upload, $content);
        }

        return self::streamer($upload);
    }

    /**
     * This function use wget in console for loading and saving files.
     *
     * @param $url_feed
     * @return FileStreamer
     * @throws Exception
     */
    public static function createWgetStreamer($url_feed)
    {
        $upload = NetworkSetting::uploadFolder('/' . NetworkSetting::parseFile() . '_network.xml');
        if (file_exists($upload)) {
            unlink($upload);
        }

        @exec("wget -O '$upload'  '$url_feed' --no-check-certificate --no-cache;");

        return self::streamer($upload);
    }

    /**
     *  create instance
     *
     * @param $file
     * @return FileStreamer
     * @throws Exception
     */
    private static function streamer($file)
    {
        return new static(
            new XmlStringStreamer(
                new UniqueNode([
                    "uniqueNode" => 'ad',
                    "checkShortClosing" => true
                ]),
                new File($file, 2048)
            )
        );
    }

    /**
     * @param \Closure $callback
     * @return FileStreamer
     */
    public function parse(\Closure $callback)
    {
        try {
            while ($node = $this->xml->getNode()) {
                $callback(new NetworkParser($this->simplexmlLoadString($node)));
            }
        } catch (Exception $exception) {
            $this->setError($exception);
        }

        return $this;
    }

    /**
     * return instance of NetworkParser
     *
     * if you want use parser with "while"
     *
     * for example
     * while ($instance = $a->spitOutNode()) {
     *  var_dump($a->title());
     * }
     *
     * @return NetworkParser|false
     */
    public function spitOutNode()
    {
        $xml = $this->xml->getNode();

        if (!$xml) {
            return false;
        }

        return new NetworkParser($this->simplexmlLoadString($xml));
    }
}